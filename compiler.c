/************************************************************************
University of Leeds
School of Computing
COMP2932- Compiler Design and Construction
The Compiler Module

I confirm that the following code has been developed and written by me and it is entirely the result of my own work.
I also confirm that I have not copied any parts of this program from another person or any other source or facilitated someone to copy this program from me.
I confirm that I will not publish the program online or share it with anyone without permission of the module leader.

Student Name: Pawel Wilczewski
Student ID: 201452111
Email: sc20pw@leeds.ac.uk
Date Work Commenced: 26.04.2022
*************************************************************************/

#ifndef NULL
#define NULL ((void *)0)
#endif

#include "compiler.h"
#include <dirent.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

static char *vmCode = NULL;

int InitCompiler ()
{
	return 1;
}

ParserInfo compile(char* dir_name)
{
	ParserInfo p;
    p.er = none;

    for (int i = 0; i < 3; i++)
    {
        InitParser(dir_name);
        DIR *d = opendir(dir_name);
        if (d != 0)
        {
            struct dirent *dir;
            while ((dir = readdir(d)) != 0)
            {
                char *filename = dir->d_name;
                if (filename[0] == '.' || (filename[StrLength(filename) - 1] != 'k' && filename[StrLength(filename) - 1] != 'K')) // no . or .. or non-jack files
                    continue;

                char file[300];
                strcpy(file, dir_name);
                strcat(file, "/");
                strcat(file, filename);
                vmCode = NULL;

//                printf("Parsing %s, stage %d/3\n", file, i + 1);
                InitLexer(file);
                p = Parse();
                StopLexer();

                if (i == 2)
                {
//                    printf("Generated vm code:\n%s", vmCode);
                    char *vmFilename = StrCopy(filename);
                    vmFilename[StrLength(filename) - 4] = 'v';
                    vmFilename[StrLength(filename) - 3] = 'm';
                    vmFilename[StrLength(filename) - 2] = '\0';
                    char vmFile[300];
                    strcpy(vmFile, dir_name);
                    strcat(vmFile, "/");
                    strcat(vmFile, vmFilename);

                    FILE *f = fopen(vmFile, "w");
                    fputs(vmCode, f);
                    fclose(f);
                }

                if (p.er != none)
                    break;
            }
            if (p.er != none)
                break;
            closedir(d);
        }
    }
    StopParser();

	return p;
}

int StopCompiler()
{


	return 1;
}

void addInstruction(char *str) {
    if (vmCode == NULL)
    {
        vmCode = (char *) malloc(sizeof(char));
        vmCode[0] = '\0';
    }

    StrCombine(&vmCode, str);
}

void endInstruction()
{
    StrCombine(&vmCode, "\n");
}

void addInstructionLine(char *str)
{
    addInstruction(str);
    endInstruction();
}

#ifndef TEST_COMPILER
int main(int argc, char *argv[])
{
    InitCompiler();
    compile(argv[1]);
    StopCompiler();
	return 1;
}
#endif

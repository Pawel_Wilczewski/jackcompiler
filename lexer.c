/************************************************************************
University of Leeds
School of Computing
COMP2932- Compiler Design and Construction
Lexer Module

I confirm that the following code has been developed and written by me and it is entirely the result of my own work.
I also confirm that I have not copied any parts of this program from another person or any other source or facilitated someone to copy this program from me.
I confirm that I will not publish the program online or share it with anyone without permission of the module leader.

Student Name: Pawel Wilczewski
Student ID: 201452111
Email: sc20pw@leeds.ac.uk
Date Work Commenced: 10.02.2022
*************************************************************************/


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "lexer.h"
#include "symbols.h"

#ifndef EOF
#define EOF (-1)
#endif

#ifndef NULL
#define NULL ((void *)0)
#endif

char *s = NULL;
int len = 0;
int line = 1;
int cindex = 0;
char file[32];
char c;

#define KEYWORDS_COUNT 21
const char *keywords[KEYWORDS_COUNT] = {"class", "constructor", "method", "function", "int",
                                        "boolean", "char", "void", "var", "static", "field",
                                        "let", "do", "if", "else", "while",
                                        "return", "true", "false", "null", "this"};

#define SYMBOLS_COUNT 19
const char *symbols[SYMBOLS_COUNT] = {"(", ")", "[", "]", "{", "}", ",", ";", "=",
                                      ".", "+", "-", "*", "/", "&", "|", "~",
                                      "<", ">"};

int IsFirstKeywordChar(char x)
{
    return x == '_' || (x >= 'A' && x <= 'Z') || (x >= 'a' && x <= 'z');
}

int IsKeywordChar(char x)
{
    return x == '_' || (x >= '0' && x <= '9') || (x >= 'A' && x <= 'Z') || (x >= 'a' && x <= 'z');
}

int IsDigit(char x)
{
    return x >= '0' && x <= '9';
}

int IsKeyword(const char *x)
{
    for (int i = 0; i < KEYWORDS_COUNT; i++)
        if (cmp(x, keywords[i]))
            return 1;
    return 0;
}

int IsSymbol(const char *x)
{
    for (int i = 0; i < SYMBOLS_COUNT; i++)
        if (cmp(x, symbols[i]))
            return 1;
    return 0;
}

char* TokenTypeStringDebug(TokenType t)
{
    switch (t)
    {
        case RESWORD: return "RESWORD";
        case ID: return "ID";
        case INT: return "INT";
        case SYMBOL: return "SYMBOL";
        case STRING: return "STRING";
        case EOFile: return "EOFile";
        case ERR: return "ERR";
        default: return "Not a recognised token type";
    }
}

void PrintTokenDebug(Token t)
{
    printf ("<%s, %i, %s, %s>\n", t.fl, t.ln , t.lx, TokenTypeStringDebug(t.tp));
}

char ReadNextChar()
{
    char c = s[cindex++];
    if (c == '\n')
        line++;
    return c;
}

Token CreateToken(char lx[128], TokenType tp, int ln)
{
    Token t;

    if (tp == EOFile)
    {
        strncpy(t.lx, "End of File", 11);
        t.lx[11] = '\0';
    }
    else
    {
        for (int i = 0; i < 128; i++)
            t.lx[i] = lx[i];
    }
    t.tp = tp;
    t.ln = ln;
    for (int i = 0; i < 32; i++)
        t.fl[i] = file[i];

    return t;
}

Token CreateErrToken(LexErrCodes ec, int ln)
{
    Token t;
    t.ec = ec;
    switch (ec)
    {
        case EofInCom:
            strncpy(t.lx, "Error: unexpected eof in comment", 32);
            t.lx[32] = '\0';
            break;
        case NewLnInStr:
            strncpy(t.lx, "Error: new line in string constant", 34);
            t.lx[34] = '\0';
            break;
        case EofInStr:
            strncpy(t.lx, "Error: unexpected eof in string constant", 40);
            t.lx[40] = '\0';
            break;
        case IllSym:
            strncpy(t.lx, "Error: illegal symbol in source file", 36);
            t.lx[36] = '\0';
            break;
        default:
            strncpy(t.lx, "Error: unknown error", 20);
            t.lx[20] = '\0';
            break;
    }
    t.tp = ERR;
    t.ln = ln;

    for (int i = 0; i < 32; i++)
        t.fl[i] = file[i];

    return t;
}

// IMPLEMENT THE FOLLOWING functions
//***********************************

// Initialise the lexer to read from source file
// file_name is the name of the source file
// This requires opening the file and making any necessary initialisations of the lexer
// If an error occurs, the function should return 0
// if everything goes well the function should return 1
int InitLexer (char* file_name)
{
    // copy the filename
    for (int i = 0; i < 32; i++)
    {
        if (file_name[i] == '\0')
        {
            file[i] = '\0';
            break;
        }

        file[i] = file_name[i];
    }

    // init line and cindex of the current char
    line = 1;
    cindex = 0;

    FILE *fp = fopen(file_name, "r");
    if (fp == NULL)
        return 0;

    // read the file one-by-one
    s = malloc(sizeof(char) * 1);
    len = 0;
    while (1)
    {
        // read next char
        char x = (char) fgetc(fp);

        s = realloc(s, sizeof(char) * (++len));
        s[len - 1] = x;
        if (x == EOF)
            break;
    }
    s = realloc(s, sizeof(char) * (len + 1));
    s[len] = '\0';

    // read the full file
//     fseek (fp, 0, SEEK_END);
//     len = ftell(fp) + 2;
//
//     // read contents and save to buffer
//     fseek (fp, 0, SEEK_SET);
//     s = (char *) malloc(sizeof(char) * len);
//     if (s == NULL)
//       return 0;
//     fread(s, sizeof(char), len - 2, fp);
//     s[len - 2] = EOF;
//     s[len - 1] = '\0';

//    printf("Read string:\n%sTHEEND\n", s);
//    printf("Last char: %c [%d]\n", s[len - 1], s[len - 1]);
//    printf("Length: %d\n", len);

    fclose (fp);

    // read the first character for GetNextToken
    c = ReadNextChar();

    return s != NULL;
}

// Get the next token from the source file
Token GetNextToken ()
{
    // this function expects to have the first character (c) already read
    while (cindex <= len)
    {
        // printf("Starting with %c\n", c);
        // read (ignore) white spaces
        while ((c == ' ' || c == '\t' || c == '\n' || c == '\r' || c =='\v' || c == '\f') && cindex < len)
            c = ReadNextChar();

        // handle comments or division symbol
        if (c == '/')
        {
            c = ReadNextChar();
            // multi/partial line comment
            if (c == '*')
            {
                char prevc;
                do {
                    prevc = c;
                    c = ReadNextChar();

                    // EOF in multi/partial line comment
                    if (c == EOF)
                        return CreateErrToken(EofInCom, line);
                } while (!(prevc == '*' && c == '/') && cindex < len);
                c = ReadNextChar();
                continue;
            }
            // single line comment
            else if (c == '/')
            {
                do {
                    c = ReadNextChar();

                    // EOF in single line comment
                    if (c == EOF)
                        return CreateErrToken(EofInCom, line);
                } while (c != '\n' && cindex < len);
                continue;
            }
            // division
            else
                // we have already read the next character, so don't read another one
                return CreateToken("/", SYMBOL, c == '\n' ? line - 1 : line);
        }

        // end of file?
        if (c == EOF)
        {
            char x[2] = {EOF, '\0'};
            return CreateToken(x, EOFile, line);
        }

        // string?
        char x[128];
        if (c == '"')
        {
            int i = 0;
            char prevc;
            do {
                prevc = c;
                c = ReadNextChar();

                // EOF in string
                if (c == EOF)
                    return CreateErrToken(EofInStr, line);

                // new line in string
                if (c == '\n')
                    return CreateErrToken(NewLnInStr, line - 1);

                if (c != '"' || (c == '"' && prevc == '\\')) // protection for >>> \" <<< in strings
                    x[i++] = c;
            } while ((c != '"' || (c == '"' && prevc == '\\')) && cindex < len && i < 128);

            i = i < 128 ? i : 127;
            x[i] = '\0';

            Token t = CreateToken(x, STRING, line);
            c = ReadNextChar();
            return t;
        }

        // keyword or identifier
        if (IsFirstKeywordChar(c))
        {
            int i = 0;
            while (IsKeywordChar(c))
            {
                x[i++] = c;
                c = ReadNextChar();
            }
            x[i] = '\0';

            // we've already read the next char
            return CreateToken(x, IsKeyword(x) ? RESWORD : ID, c == '\n' ? line - 1 : line);
        }

        // number
        if (IsDigit(c))
        {
            int i = 0;
            while (IsDigit(c))
            {
                x[i++] = c;
                c = ReadNextChar();
            }
            x[i] = '\0';

            // we've already read the next char
            return CreateToken(x, INT, c == '\n' ? line - 1 : line);
        }

        // symbol otherwise
        x[0] = c;
        x[1] = '\0';
        if (IsSymbol(x))
        {
            Token t = CreateToken(x, SYMBOL, line);
            c = ReadNextChar();
            return t;
        }

        Token t = CreateErrToken(IllSym, line);
        c = ReadNextChar();
        return t;
    }

    return CreateErrToken(-1, line);
}

// peek (look) at the next token in the source file without removing it from the stream
Token PeekNextToken ()
{
    char prev_c = c;
    int previous_line = line;
    int previous_cindex = cindex;
    Token t = GetNextToken();
    c = prev_c;
    cindex = previous_cindex;
    line = previous_line;
    return t;
}

// clean out at end, e.g. close files, free memory, ... etc
int StopLexer ()
{
    free(s);
    s = NULL;
    return 0;
}

//// do not remove the next line
//#ifndef TEST
//int main ()
//{
//  InitLexer ("Main.jack");
//  Token t = GetNextToken ();
//  int i = 0;
//  while (t.tp != EOFile)
//  {
//    PrintTokenDebug(t);
//    t  = GetNextToken();
//    i++;
//  }
//  PrintTokenDebug(t);
//  StopLexer ();
//
//	return 0;
//}
//// do not remove the next line
//#endif

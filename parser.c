#include <stdlib.h>
#include <stdio.h>

#include "lexer.h"
#include "parser.h"
#include "symbols.h"
#include "compiler.h"

#ifndef NULL
#define NULL ((void *)0)
#endif

// STATIC VARS
static Token token;
static ParserInfo pi;
static SymbolTable *classTable = NULL;
static MethodTables *methodTables = NULL;
static TypeTable *typeTable = NULL;
static ArgTable *argTable = NULL;
char *methodId = NULL;
static int labelCounter = 0;
static char *calledId = NULL;
static Symbol *calledIdSymbol = NULL;
//static int calledFuncArgsCount = 0;
static char *calledFuncThis = NULL;
static int declaredSubKind = CONSTRUCTOR;

static char *className = NULL;
static int initState = -1;


// HELPER
#define START if (pi.er != none) return;
#define ERROR(err) { error_(err); return; }
#define CALL(func) { func(); if (pi.er != none) return; }
#define GOOD ;
#define methodTable FindSymbolTable(methodTables, methodId)

static char *KindToStackName(SymbolKind kind)
{
    switch(kind)
    {
        case STATIC:
            return "static ";
        case FIELD:
            return "this ";
        case ARGUMENT:
            return "argument ";
        case VAR:
            return "local ";
        default:
            return "ERROR-STACK-NAME ";
    }
}

static char* TokenTypeStringDebug(TokenType t)
{
    switch (t)
    {
        case RESWORD: return "RESWORD";
        case ID: return "ID";
        case INT: return "INT";
        case SYMBOL: return "SYMBOL";
        case STRING: return "STRING";
        case EOFile: return "EOFile";
        case ERR: return "ERR";
        default: return "Not a recognised token type";
    }
}

static void PrintTokenDebug(Token t)
{
    printf("<%s, %i, %s, %s>\n", t.fl, t.ln, t.lx, TokenTypeStringDebug(t.tp));
}

static char* ErrorString_ (SyntaxErrors e)
{
    switch (e)
    {
        case none: return "no errors";
        case lexerErr: return "lexer error";
        case classExpected: return "keyword class expected";
        case idExpected: return "identifier expected";
        case openBraceExpected:	return "{ expected";
        case closeBraceExpected: return "} expected";
        case memberDeclarErr: return "class member declaration must begin with static, field, constructor , function , or method";
        case classVarErr: return "class variables must begin with field or static";
        case illegalType: return "a type must be int, char, boolean, or identifier";
        case semicolonExpected: return "; expected";
        case subroutineDeclarErr: return "subrouting declaration must begin with constructor, function, or method";
        case openParenExpected: return "( expected";
        case closeParenExpected: return ") expected";
        case closeBracketExpected: return "] expected";
        case equalExpected: return "= expected";
        case syntaxError: return "syntax error";
            // semantic errors
        case undecIdentifier: return "undeclared identifier";
        case redecIdentifier: return "redeclaration of identifier";
        default: return "not a valid error code";
    }
}

static void error_(SyntaxErrors err)
{
    pi.er = err;
    pi.tk = token;
    printf("Parsing error: %s; Token was: ", ErrorString_(err));
    PrintTokenDebug(token);
}

// MAIN FUNCTIONS
void memberDeclar(); void classVarDeclar(); void subroutineDeclar(); void type(); void idDotOptional_read();
void paramList(); void subroutineBody(); void statement(); void varDeclarStatement();
void letStatement(); void ifStatement(); void whileStatement();void doStatement();
void subroutineCall(); void expressionList(); void returnStatement(); void expression();
void relationalExpression(); void arithmeticExpression(); void term(); void factor(); void operand();

// each function will always have to get its next corresponding token - convention
void classDeclar()
{
    START

    token = GetNextToken();
    if (cmp(token.lx, "class"))
    {
        token = GetNextToken();
        if (token.tp == ID)
        {
            className = StrCopy(token.lx);
            if (initState == 0)
                AddType(typeTable, className);

            token = GetNextToken();
            if (cmp(token.lx, "{"))
            {
                token = PeekNextToken();
                while (!cmp(token.lx, "}"))
                {
                    CALL(memberDeclar)
                    token = PeekNextToken();
                }

                token = GetNextToken();
                if (cmp(token.lx, "}"))
                    GOOD
                else
                    ERROR(closeBraceExpected)
            }
            else
                ERROR(openBraceExpected)
        }
        else
            ERROR(idExpected)
    }
    else
        ERROR(classExpected)
}

void memberDeclar()
{
    START

    token = PeekNextToken();
    if (cmp(token.lx, "static") || cmp(token.lx, "field"))
    {
        CALL(classVarDeclar)
    }
    else if (cmp(token.lx, "constructor") || cmp(token.lx, "function") || cmp(token.lx, "method"))
    {
        CALL(subroutineDeclar)
    }
    else
        ERROR(memberDeclarErr)
}

void classVarDeclar()
{
    START

    token = GetNextToken();
    if (cmp(token.lx, "static") || cmp(token.lx, "field"))
    {
        SymbolKind kind;
        if (cmp(token.lx, "static"))
            kind = STATIC;
        else
            kind = FIELD;

        CALL(type)
        char *tp = StrCopy(token.lx);

        token = GetNextToken();
        while (token.tp == ID)
        {
            if (initState == 0 && !Insert(classTable, className, token.lx, tp, kind))
                ERROR(redecIdentifier)

            token = GetNextToken();
            if (cmp(token.lx, ";"))
                break;
            else if (cmp(token.lx, ","))
                token = GetNextToken();
            else
            ERROR(syntaxError) // TODO: maybe more specific? doesn't seem like there's one
        }
        if (cmp(token.lx, ";"))
            GOOD
        else
            ERROR(idExpected)
    }
    else
        ERROR(classVarErr)
}

void type()
{
    START

    token = GetNextToken();
    if (initState == 1)
    {
        if (TypeExists(typeTable, token.lx))
            GOOD
        else
            ERROR(undecIdentifier)
    }
}

void subroutineDeclar()
{
    START

    labelCounter = 0;
    token = GetNextToken();
    if (cmp(token.lx, "constructor") || cmp(token.lx, "function") || cmp(token.lx, "method"))
    {
        SymbolKind kind;
        if (cmp(token.lx, "constructor"))
            kind = CONSTRUCTOR;
        else if (cmp(token.lx, "function"))
            kind = FUNCTION;
        else
            kind = METHOD;
        declaredSubKind = kind;

        token = PeekNextToken();
        if (cmp(token.lx, "void"))
        {
            token = GetNextToken();
            GOOD
        }
        else
            CALL(type)

        char *tp = StrCopy(token.lx);
        token = GetNextToken();
        if (token.tp == ID)
        {
            if (initState == 0 && !Insert(classTable, className, token.lx, tp, kind))
                ERROR(redecIdentifier)

            methodId = StrCopy("");
            StrCombine(&methodId, className);
            StrCombine(&methodId, ".");
            StrCombine(&methodId, token.lx);

            if (initState == 1)
                AddMethodTable(methodTables, NewSymbolTable(), methodId);
            else if (initState == 2)
            {
                // add func description
                addInstruction("function ");
                addInstruction(methodId);
                addInstruction(" ");
                addInstruction(IntToStr(FilterTableByKind(methodTable, VAR)->length));
                endInstruction();

                // add initial stuff for constructors and methods
                switch(kind)
                {
                    case METHOD:
                        // set 'this' to correct value
                        addInstructionLine("push argument 0");
                        addInstructionLine("pop pointer 0");
                        break;
                    case CONSTRUCTOR:
                        // memory allocation
                        addInstruction("push constant ");
                        addInstruction(IntToStr(FilterTableByClass(FilterTableByKind(classTable, FIELD), className)->length));
                        endInstruction();
                        addInstructionLine("call Memory.alloc 1");
                        addInstructionLine("pop pointer 0");
                        break;
                    default:
                        break;
                }
            }

            token = GetNextToken();
            if (cmp(token.lx, "("))
            {
                CALL(paramList)
                token = GetNextToken();
                if (cmp(token.lx, ")"))
                {
                    CALL(subroutineBody)
                }
                else
                    ERROR(closeParenExpected)
            }
            else
                ERROR(openParenExpected)
        }
        else
            ERROR(idExpected)
    }
    else
        ERROR(subroutineDeclarErr)
}

void paramList()
{
    START
    int argCount = 0;

    if (initState == 1 && declaredSubKind == METHOD)
    {
        if (!Insert(methodTable, className, "this", className, ARGUMENT))
            ERROR(redecIdentifier)
        else
            argCount++;
    }

    token = PeekNextToken();
    if (cmp(token.lx, ")"))
    {

    }
    else
    {
        if (cmp(token.lx, "{"))
            ERROR(closeParenExpected) // special case: it's required to say this instead of invalid type in case of unclosed parenthesis

        CALL(type)
        char *tp = StrCopy(token.lx);
        token = GetNextToken();
        if (token.tp == ID)
        {
            if (initState == 1 && !Insert(methodTable, className, token.lx, tp, ARGUMENT))
                ERROR(redecIdentifier)
            argCount++;
        }
        else
            ERROR(idExpected)

        token = PeekNextToken();
        while (cmp(token.lx, ","))
        {
            token = GetNextToken();
            CALL(type)

            token = GetNextToken();
            if (token.tp == ID)
            {
                if (initState == 1 && !Insert(methodTable, className, token.lx, tp, ARGUMENT))
                    ERROR(redecIdentifier)
                argCount++;
            }
            else
                ERROR(idExpected)

            token = PeekNextToken();
        }

        if (cmp(token.lx, ")"))
            GOOD
        else
            ERROR(closeParenExpected)
    }

    if (initState == 1)
        AddArgCount(argTable, methodId, argCount);
}

void subroutineBody()
{
    START

    token = GetNextToken();
    if (cmp(token.lx, "{"))
    {
        token = PeekNextToken();
        while (!cmp(token.lx, "}"))
        {
            CALL(statement)
            token = PeekNextToken();
        }
    }
    else
        ERROR(openBraceExpected)

    token = GetNextToken();
    if (cmp(token.lx, "}"))
        GOOD
    else
        ERROR(closeBraceExpected)
}

void statement()
{
    START

    token = PeekNextToken();
    if (cmp(token.lx, "var"))
        CALL(varDeclarStatement)
    else if (cmp(token.lx, "let"))
        CALL(letStatement)
    else if (cmp(token.lx, "if"))
        CALL(ifStatement)
    else if (cmp(token.lx, "while"))
        CALL(whileStatement)
    else if (cmp(token.lx, "do"))
        CALL(doStatement)
    else if (cmp(token.lx, "return"))
        CALL(returnStatement)
    else
        ERROR(syntaxError)
}

void varDeclarStatement()
{
    START

    token = GetNextToken();
    if (cmp(token.lx, "var"))
    {
        CALL(type)
        char *tp = StrCopy(token.lx);

        token = GetNextToken();
        while (token.tp == ID)
        {
            if (initState == 1 && !Insert(methodTable, className, token.lx, tp, VAR))
                ERROR(redecIdentifier)

            token = GetNextToken();
            if (cmp(token.lx, ";"))
                break;
            else if (cmp(token.lx, ","))
                token = GetNextToken();
            else
                ERROR(syntaxError) // TODO: maybe more specific? doesn't seem like there's one
        }
        if (cmp(token.lx, ";"))
            GOOD
        else
            ERROR(idExpected)
    }
    else
        ERROR(syntaxError)
}

void letStatement()
{
    START

    token = GetNextToken();
    if (cmp(token.lx, "let"))
    {
        int arrayLet = 0;
        token = GetNextToken();
        if (token.tp == ID)
        {
            char *name = StrCopy(token.lx);

            Symbol *s1 = LookUp(methodTable, className, token.lx);
            Symbol *s2 = LookUp(classTable, className, token.lx);

            if (initState == 1)
                if (!s1 && !s2)
                    ERROR(undecIdentifier)

            token = GetNextToken();
            if (cmp(token.lx, "["))
            {
                while (cmp(token.lx, "["))
                {
                    CALL(expression)

                    token = GetNextToken();
                    if (cmp(token.lx, "]"))
                    {
                        if (initState == 2)
                        {
                            Symbol *s = s1;
                            if (s == NULL)
                                s = s2;

                            addInstruction("push ");
                            addInstruction(KindToStackName(s->kind));
                            addInstructionLine(IntToStr(s->id));

                            addInstructionLine("add");
                            arrayLet = 1;
                        }
                    }
                    else
                        ERROR(closeBracketExpected)

                    token = GetNextToken();
                }
            }
            else if (!cmp(token.lx, "="))
                ERROR(equalExpected)

            // equals should be now the token
            if (cmp(token.lx, "="))
            {
                GOOD
            }
            else
                ERROR(equalExpected)

            CALL(expression)

            token = GetNextToken();
            if (cmp(token.lx, ";"))
            {
                // assign the value
                if (initState == 2)
                {
                    if (!arrayLet) // normal handling
                    {
                        addInstruction("pop ");
                        Symbol *s = LookUp(methodTable, className, name);
                        if (s == NULL)
                            s = LookUp(classTable, className, name);

                        // mustn't be null at this stage if passed previous stages
                        addInstruction(KindToStackName(s->kind));
                        addInstructionLine(IntToStr(s->id));
                    }
                    else // array handling
                    {
                        addInstructionLine("pop temp 0");
                        addInstructionLine("pop pointer 1");
                        addInstructionLine("push temp 0");
                        addInstructionLine("pop that 0");
                    }
                }
            }
            else
                ERROR(semicolonExpected)
        }
        else
            ERROR(idExpected)
    }
    else
        ERROR(syntaxError)
}

void ifStatement()
{
    START

    token = GetNextToken();
    if (cmp(token.lx, "if"))
    {
        token = GetNextToken();
        if (cmp(token.lx, "("))
        {
            CALL(expression)
            // the expression is now on the stack, so later we can go to either true or false statement

            token = GetNextToken();
            if (cmp(token.lx, ")"))
            {
                char *trueLabel = StrCopy("IF_TRUE");
                char *falseLabel = StrCopy("IF_FALSE");
                char *endLabel = StrCopy("IF_END");
                if (initState == 2) {
                    int num = labelCounter++;
                    StrCombine(&trueLabel, IntToStr(num));
                    StrCombine(&falseLabel, IntToStr(num));
                    StrCombine(&endLabel, IntToStr(num));

                    addInstruction("if-goto ");
                    addInstruction(trueLabel);
                    endInstruction();
                    addInstruction("goto ");
                    addInstruction(falseLabel);
                    endInstruction();
                    addInstruction("label ");
                    addInstruction(trueLabel);
                    endInstruction();
                }

                CALL(subroutineBody)



                token = PeekNextToken();
                if (cmp(token.lx, "else"))
                {
                    if (initState == 2)
                    {
                        addInstruction("goto ");
                        addInstruction(endLabel);
                        endInstruction();

                        addInstruction("label ");
                        addInstruction(falseLabel);
                        endInstruction();
                    }

                    token = GetNextToken();
                    CALL(subroutineBody)

                    if (initState == 2)
                    {
                        addInstruction("label ");
                        addInstruction(endLabel);
                        endInstruction();
                    }
                }
                else
                {
                    addInstruction("label ");
                    addInstruction(falseLabel);
                    endInstruction();
                }
            }
            else
                ERROR(closeParenExpected)
        }
        else
            ERROR(openParenExpected)
    }
    else
        ERROR(syntaxError)
}

void whileStatement()
{
    START

    token = GetNextToken();
    if (cmp(token.lx, "while"))
    {
        token = GetNextToken();
        if (cmp(token.lx, "("))
        {
            char *expLabel = NULL;
            char *whileLabel = NULL;
            char *endLabel = NULL;
            if (initState == 2)
            {
                int num = labelCounter++;
                expLabel = StrCopy("WHILE_EXP");
                endLabel = StrCopy("WHILE_END");
                StrCombine(&expLabel, IntToStr(num));
                StrCombine(&endLabel, IntToStr(num));

                addInstruction("label ");
                addInstruction(expLabel);
                endInstruction();
            }

            CALL(expression)

            if (initState == 2)
            {
                addInstructionLine("not");
                addInstruction("if-goto ");
                addInstruction(endLabel);
                endInstruction();
            }

            token = GetNextToken();
            if (cmp(token.lx, ")"))
            {
                CALL(subroutineBody)

                if (initState == 2)
                {
                    addInstruction("goto ");
                    addInstruction(expLabel);
                    endInstruction();
                    addInstruction("label ");
                    addInstruction(endLabel);
                    endInstruction();
                }
            }
            else
                ERROR(closeParenExpected)
        }
        else
            ERROR(openParenExpected)
    }
    else
        ERROR(syntaxError)
}

void doStatement()
{
    START

    token = GetNextToken();
    if (cmp(token.lx, "do"))
    {
        CALL(subroutineCall)

        token = GetNextToken();
        if (cmp(token.lx, ";"))
        {
            // cheat "assign" the value
            if (initState == 2)
                addInstructionLine("pop temp 0");
            GOOD
        }
        else
            ERROR(semicolonExpected)
    }
    else
        ERROR(syntaxError)
}

void subroutineCall()
{
    START

    calledId = NULL;
    token = GetNextToken();
    Token firstToken;
    if (token.tp == ID)
    {
        CALL(idDotOptional_read)
        char *funcId;
        char *funcThis;
        if (initState == 2)
        {
            funcId = StrCopy(calledId); // TODO: is this necessary in this case?
            funcThis = StrCopy(calledFuncThis);
        }

        token = GetNextToken();
        if (cmp(token.lx, "("))
        {
            if (initState == 2)
            {
                if (funcThis != NULL)
                {
                    addInstruction("push ");
                    addInstruction(funcThis); // TODO: redo this funcThis thing as we can access this by looking up the method table
                    endInstruction();
                }
            }

            CALL(expressionList)

            token = GetNextToken();
            if (cmp(token.lx, ")"))
            {
                if (initState == 2)
                {
                    addInstruction("call ");
                    addInstruction(funcId);
                    addInstruction(" ");
                    addInstruction(IntToStr(LookUpArgCount(argTable, funcId)->argCount));
                    endInstruction();
                }
                GOOD
            }
            else
                ERROR(closeParenExpected)
        }
        else
            ERROR(openParenExpected)
    }
    else
        ERROR(idExpected)
}

void expressionList()
{
    START

    token = PeekNextToken();
    if (cmp(token.lx, ")"))
        GOOD
    else
    {
        CALL(expression)

        token = PeekNextToken();
        while (cmp(token.lx, ","))
        {
            token = GetNextToken();
            CALL(expression)
            token = PeekNextToken();
        }

        if (cmp(token.lx, ")"))
            GOOD
        else
            ERROR(syntaxError)
    }
}

void returnStatement()
{
    START

    token = GetNextToken();
    if (cmp(token.lx, "return"))
    {
        token = PeekNextToken();
        if (cmp(token.lx, ";"))
        {
            token = GetNextToken();

            // void return - return 0 by default
            if (initState == 2)
            {
                addInstructionLine("push constant 0");
                addInstructionLine("return");
            }
        }
        else
        {
            if (cmp(token.lx, "}"))
                ERROR(semicolonExpected) // again this is expected instead of a syntax error in this case

            CALL(expression)
            if (initState == 2)
            {
                addInstructionLine("return");
            }

            token = GetNextToken();
            if (cmp(token.lx, ";"))
                GOOD
            else
                ERROR(semicolonExpected)
        }
    }
    else
        ERROR(syntaxError)
}

void expression()
{
    START

    CALL(relationalExpression)

    token = PeekNextToken();
    while (cmp(token.lx, "|") || cmp(token.lx, "&"))
    {
        token = GetNextToken();
        char *op = StrCopy(token.lx);
        CALL(relationalExpression)

        if (initState == 2)
        {
            if (cmp(op, "|"))
                addInstructionLine("or");
            else if (cmp(op, "&"))
                addInstructionLine("and");
        }

        token = PeekNextToken();
    }
}

void relationalExpression()
{
    START

    CALL(arithmeticExpression)

    token = PeekNextToken();
    while (cmp(token.lx, "=") || cmp(token.lx, ">") || cmp(token.lx, "<"))
    {
        token = GetNextToken();
        char *op = StrCopy(token.lx);
        CALL(arithmeticExpression)

        if (initState == 2)
        {
            if (cmp(op, "="))
                addInstructionLine("eq");
            else if (cmp(op, ">"))
                addInstructionLine("gt");
            else if (cmp(op, "<"))
                addInstructionLine("lt");
        }

        token = PeekNextToken();
    }
}

void arithmeticExpression()
{
    START

    CALL(term)

    token = PeekNextToken();
    while (cmp(token.lx, "+") || cmp(token.lx, "-"))
    {
        token = GetNextToken();
        char *op = StrCopy(token.lx);
        CALL(term)

        if (initState == 2)
        {
            if (cmp(op, "+"))
                addInstructionLine("add");
            else if (cmp(op, "-"))
                addInstructionLine("sub");
        }

        token = PeekNextToken();
    }
}

void term()
{
    START

    CALL(factor)

    token = PeekNextToken();
    while (cmp(token.lx, "*") || cmp(token.lx, "/"))
    {
        token = GetNextToken();
        char *op = StrCopy(token.lx);
        CALL(factor)

        if (initState == 2)
        {
            if (cmp(op, "*"))
                addInstructionLine("call Math.multiply 2");
            else if (cmp(op, "/"))
                addInstructionLine("call Math.divide 2");
        }

        token = PeekNextToken();
    }
}

void factor()
{
    START

    token = PeekNextToken();
    char *op = StrCopy(token.lx);
    if (cmp(token.lx, "-") || cmp(token.lx, "~"))
    {
        GetNextToken();
        GOOD
    }

    CALL(operand)

    if (initState == 2)
    {
        if (cmp(op, "-"))
            addInstructionLine("neg");
        else if (cmp(op, "~"))
            addInstructionLine("not");
    }
}

void operand()
{
    START

    token = GetNextToken();
    if (token.tp == ID)
    {
        CALL(idDotOptional_read)
        char *funcId;
        char *funcThis;
        if (initState == 2)
        {
            if (calledIdSymbol->kind == METHOD || calledIdSymbol->kind == FUNCTION || calledIdSymbol->kind == CONSTRUCTOR)
            {
                funcId = StrCopy(calledId);
                funcThis = StrCopy(calledFuncThis);
            }
        }

        token = PeekNextToken();
        if (cmp(token.lx, "[")) // array access
        {
            Symbol *s = NULL; // remember the original symbol
            if (initState == 2)
            {
                s = (Symbol *) malloc(sizeof *s);
                s->kind = calledIdSymbol->kind;
                s->id = calledIdSymbol->id;
            }

            token = GetNextToken();

            CALL(expression)
            token = GetNextToken();
            if (cmp(token.lx, "]"))
            {
                if (initState == 2)
                {
                    addInstruction("push ");
                    addInstruction(KindToStackName(s->kind));
                    addInstructionLine(IntToStr(s->id));

                    addInstructionLine("add");

                    addInstructionLine("pop pointer 1");
                    addInstructionLine("push that 0");
                }
            }
            else
                ERROR(closeBracketExpected)
        }
        else if (cmp(token.lx, "(")) // func/method/constructor call
        {
            token = GetNextToken();

            if (initState == 2)
            {
                if (funcThis != NULL)
                {
                    addInstruction("push ");
                    addInstruction(funcThis); // TODO: redo this funcThis thing as we can access this by looking up the method table
                    endInstruction();
                }
            }

            CALL(expressionList)

            token = GetNextToken();
            if (cmp(token.lx, ")"))
            {
                if (initState == 2)
                {
                    addInstruction("call ");
                    addInstruction(funcId);
                    addInstruction(" ");
                    addInstruction(IntToStr(LookUpArgCount(argTable, funcId)->argCount));
                    endInstruction();
                }
            }
            else
                ERROR(closeParenExpected)
        }
        else // just a variable
        {
            if (initState == 2)
            {
                addInstruction("push ");
                addInstruction(KindToStackName(calledIdSymbol->kind));
                addInstruction(IntToStr(calledIdSymbol->id));
                endInstruction();
            }
        }
    }
    else if (cmp(token.lx, "(")) // parenthesis
    {
        CALL(expression)

        token = GetNextToken();
        if (cmp(token.lx, ")"))
        {
            GOOD
        }
        else
        ERROR(closeParenExpected)
    }
    else if (token.tp == INT)
    {
        if (initState == 2)
        {
            addInstruction("push constant ");
            addInstruction(token.lx);
            endInstruction();
        }
    }
    else if (token.tp == STRING)
    {
        if (initState == 2)
        {
            int len = StrLength(token.lx);
            addInstruction("push constant ");
            addInstruction(IntToStr(len));
            endInstruction();
            addInstructionLine("call String.new 1");

            for (int i = 0; i < len; i++)
            {
                addInstruction("push constant ");
                addInstruction(IntToStr(token.lx[i]));
                endInstruction();
                addInstructionLine("call String.appendChar 2");
            }
        }
    }
    else if (cmp(token.lx, "true") || cmp(token.lx, "false") || cmp(token.lx, "null") || cmp(token.lx, "this"))
    {
        if (initState == 2)
        {
            if (cmp(token.lx, "true"))
            {
                addInstructionLine("push constant 0");
                addInstructionLine("not");
            }
            else if (cmp(token.lx, "false"))
            {
                addInstructionLine("push constant 0");
            }
            else if (cmp(token.lx, "null"))
            {
                addInstructionLine("push constant 0");
            }
            else if (cmp(token.lx, "this"))
            {
                addInstructionLine("push pointer 0");
            }
        }
    }
    else
        ERROR(syntaxError)
}

void idDotOptional_read()
{
    START

    Token firstToken = token;
    token = PeekNextToken();
    if (cmp(token.lx, "."))
    {
        token = GetNextToken();
        token = GetNextToken();
        if (token.tp == ID)
        {
            if (initState > 0)
            {
                // figure out the type (class) of the thing
                char *tp;
                // class name is the base - func/variable
                if (TypeExists(typeTable, firstToken.lx) || ClassExists(classTable, firstToken.lx))
                {
                    tp = firstToken.lx;
                    calledFuncThis = NULL;
                    calledId = StrCopy(tp);
                    StrCombine(&calledId, ".");
                    StrCombine(&calledId, token.lx);

                    calledIdSymbol = LookUp(classTable, firstToken.lx, token.lx);
                }
                else
                {
                    // field is the base - method/variable
                    Symbol *methodPossible = LookUp(classTable, className, firstToken.lx);
                    // argument is the base - method/variable
                    Symbol *argumentPossible = LookUp(methodTable, className, firstToken.lx);
                    if (methodPossible != NULL)
                    {
                        tp = methodPossible->type;
                        calledFuncThis = StrCopy("this ");
                        StrCombine(&calledFuncThis, IntToStr(methodPossible->id));
                    }
                    else if (argumentPossible != NULL)
                    {
                        tp = argumentPossible->type;
                        calledFuncThis = StrCopy(KindToStackName(argumentPossible->kind));
                        StrCombine(&calledFuncThis, IntToStr(argumentPossible->id));
                    }
                    else
                    {
                        token = firstToken;
                        ERROR(undecIdentifier)
                    }

                    calledId = StrCopy(tp);
                    StrCombine(&calledId, ".");
                    StrCombine(&calledId, token.lx);
                }

                Symbol *symbol = LookUp(classTable, tp, token.lx);
                if (symbol == NULL)
                    ERROR(undecIdentifier)
                calledIdSymbol = symbol;
            }
            GOOD
        }
        else
            ERROR(idExpected)
    }
    else
    {
        // current class scope method/function/variable
        token = firstToken;
        if (initState > 0)
        {
            Symbol *s1 = LookUp(classTable, className, token.lx);
            Symbol *s2 = LookUp(methodTable, className, token.lx);
            if (!s1 && !s2)
                ERROR(undecIdentifier)

            Symbol *s;
            if (s1 != NULL)
                s = s1;
            else
                s = s2;

            if (s->kind == METHOD)
                calledFuncThis = StrCopy("pointer 0");
            calledId = StrCopy(className);
            StrCombine(&calledId, ".");
            StrCombine(&calledId, token.lx);
            calledIdSymbol = s;
        }
    }
}

int InitParser(char* file_name)
{
    if (initState < 0)
    {
        classTable = NewSymbolTable();
        methodTables = NewMethodTables();
        Insert(classTable, "Math", "init", "void", FUNCTION);
        Insert(classTable, "Math", "abs", "int", FUNCTION);
        Insert(classTable, "Math", "multiply", "int", FUNCTION);
        Insert(classTable, "Math", "divide", "int", FUNCTION);
        Insert(classTable, "Math", "min", "int", FUNCTION);
        Insert(classTable, "Math", "max", "int", FUNCTION);
        Insert(classTable, "Math", "sqrt", "int", FUNCTION);

        Insert(classTable, "Array", "new", "Array", FUNCTION);
        Insert(classTable, "Array", "dispose", "void", METHOD);

        Insert(classTable, "Memory", "init", "void", FUNCTION);
        Insert(classTable, "Memory", "peek", "int", FUNCTION);
        Insert(classTable, "Memory", "poke", "void", FUNCTION);
        Insert(classTable, "Memory", "alloc", "Array", FUNCTION);
        Insert(classTable, "Memory", "deAlloc", "void", FUNCTION);

        Insert(classTable, "Screen", "init", "void", FUNCTION);
        Insert(classTable, "Screen", "clearScreen", "void", FUNCTION);
        Insert(classTable, "Screen", "setColor", "void", FUNCTION);
        Insert(classTable, "Screen", "drawPixel", "void", FUNCTION);
        Insert(classTable, "Screen", "drawLine", "void", FUNCTION);
        Insert(classTable, "Screen", "drawRectangle", "void", FUNCTION);
        Insert(classTable, "Screen", "drawCircle", "void", FUNCTION);

        Insert(classTable, "Keyboard", "init", "void", FUNCTION);
        Insert(classTable, "Keyboard", "keyPressed", "char", FUNCTION);
        Insert(classTable, "Keyboard", "readChar", "char", FUNCTION);
        Insert(classTable, "Keyboard", "readLine", "String", FUNCTION);
        Insert(classTable, "Keyboard", "readInt", "int", FUNCTION);

        Insert(classTable, "Sys", "init", "void", FUNCTION);
        Insert(classTable, "Sys", "halt", "void", FUNCTION);
        Insert(classTable, "Sys", "error", "void", FUNCTION);
        Insert(classTable, "Sys", "wait", "void", FUNCTION);

        Insert(classTable, "Output", "init", "void", FUNCTION);
        Insert(classTable, "Output", "initMap", "void", FUNCTION);
        Insert(classTable, "Output", "create", "void", FUNCTION);
        Insert(classTable, "Output", "getMap", "void", FUNCTION);
        Insert(classTable, "Output", "moveCursor", "void", FUNCTION);
        Insert(classTable, "Output", "printChar", "void", FUNCTION);
        Insert(classTable, "Output", "printString", "void", FUNCTION);
        Insert(classTable, "Output", "printInt", "void", FUNCTION);
        Insert(classTable, "Output", "println", "void", FUNCTION);
        Insert(classTable, "Output", "backSpace", "void", FUNCTION);

        Insert(classTable, "String", "new", "String", FUNCTION);
        Insert(classTable, "String", "dispose", "void", METHOD);
        Insert(classTable, "String", "length", "int", METHOD);
        Insert(classTable, "String", "charAt", "char", METHOD);
        Insert(classTable, "String", "setCharAt", "void", METHOD);
        Insert(classTable, "String", "appendChar", "String", METHOD);
        Insert(classTable, "String", "eraseLastChar", "void", METHOD);
        Insert(classTable, "String", "intValue", "int", METHOD);
        Insert(classTable, "String", "setInt", "void", METHOD);
        Insert(classTable, "String", "newLine", "char", FUNCTION);
        Insert(classTable, "String", "backSpace", "char", FUNCTION);
        Insert(classTable, "String", "doubleQuote", "char", FUNCTION);

        typeTable = NewTypeTable();
        AddType(typeTable, "int");
        AddType(typeTable, "char");
        AddType(typeTable, "boolean");
        AddType(typeTable, "String");
        AddType(typeTable, "Array");

        argTable = NewArgTable();
        AddArgCount(argTable, "Math.init", 0);
        AddArgCount(argTable, "Math.abs", 1);
        AddArgCount(argTable, "Math.multiply", 2);
        AddArgCount(argTable, "Math.divide", 2);
        AddArgCount(argTable, "Math.min", 2);
        AddArgCount(argTable, "Math.max", 2);
        AddArgCount(argTable, "Math.sqrt", 1);

        AddArgCount(argTable, "Array.new", 1);
        AddArgCount(argTable, "Array.dispose", 1);

        AddArgCount(argTable, "Memory.init", 0);
        AddArgCount(argTable, "Memory.peek", 1);
        AddArgCount(argTable, "Memory.poke", 2);
        AddArgCount(argTable, "Memory.alloc", 1);
        AddArgCount(argTable, "Memory.deAlloc", 1);

        AddArgCount(argTable, "Screen.init", 0);
        AddArgCount(argTable, "Screen.clearScreen", 0);
        AddArgCount(argTable, "Screen.setColor", 1);
        AddArgCount(argTable, "Screen.drawPixel", 2);
        AddArgCount(argTable, "Screen.drawLine", 4);
        AddArgCount(argTable, "Screen.drawRectangle", 4);
        AddArgCount(argTable, "Screen.drawCircle", 3);

        AddArgCount(argTable, "Keyboard.init", 0);
        AddArgCount(argTable, "Keyboard.keyPressed", 0);
        AddArgCount(argTable, "Keyboard.readChar", 0);
        AddArgCount(argTable, "Keyboard.readLine", 1);
        AddArgCount(argTable, "Keyboard.readInt", 1);

        AddArgCount(argTable, "Sys.init", 0);
        AddArgCount(argTable, "Sys.halt", 0);
        AddArgCount(argTable, "Sys.error", 1);
        AddArgCount(argTable, "Sys.wait", 1);

        AddArgCount(argTable, "Output.init", 0);
        AddArgCount(argTable, "Output.initMap", 0);
        AddArgCount(argTable, "Output.create", 12);
        AddArgCount(argTable, "Output.getMap", 1);
        AddArgCount(argTable, "Output.moveCursor", 2);
        AddArgCount(argTable, "Output.printChar", 1);
        AddArgCount(argTable, "Output.printString", 1);
        AddArgCount(argTable, "Output.printInt", 1);
        AddArgCount(argTable, "Output.println", 0);
        AddArgCount(argTable, "Output.backSpace", 0);

        AddArgCount(argTable, "String.new", 1);
        AddArgCount(argTable, "String.dispose", 1);
        AddArgCount(argTable, "String.length", 1);
        AddArgCount(argTable, "String.charAt", 2);
        AddArgCount(argTable, "String.setCharAt", 3);
        AddArgCount(argTable, "String.appendChar", 2);
        AddArgCount(argTable, "String.eraseLastChar", 1);
        AddArgCount(argTable, "String.intValue", 1);
        AddArgCount(argTable, "String.setInt", 2);
        AddArgCount(argTable, "String.newLine", 0);
        AddArgCount(argTable, "String.backSpace", 0);
        AddArgCount(argTable, "String.doubleQuote", 0);
    }
    initState++;
	return 1;
}

ParserInfo Parse()
{
    pi.er = none;

	// implement the function
    token = PeekNextToken();

    if (token.tp == ERR)
        error_(lexerErr);
    else if (token.tp != RESWORD)
        error_(classExpected);
    else
    {
        classDeclar();
        pi.tk = token;
    }

	return pi;
}

int StopParser()
{
    initState = -1;
	return 1;
}

//#ifndef TEST_PARSER
//int main ()
//{
//    return 1;
//}
//#endif

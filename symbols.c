
/************************************************************************
University of Leeds
School of Computing
COMP2932- Compiler Design and Construction
The Symbol Tables Module

I confirm that the following code has been developed and written by me and it is entirely the result of my own work.
I also confirm that I have not copied any parts of this program from another person or any other source or facilitated someone to copy this program from me.
I confirm that I will not publish the program online or share it with anyone without permission of the module leader.

Student Name: Pawel Wilczewski
Student ID: 201452111
Email: sc20pw@leeds.ac.uk
Date Work Commenced: 26.04.2022
*************************************************************************/

#include "symbols.h"
#include "stdlib.h"
#include "stdio.h"

#ifndef NULL
#define NULL ((void *)0)
#endif

int cmp(const char *str1, const char *str2)
{
    if (str1 == NULL || str2 == NULL)
        return 0;

    return !strcmp(str1, str2);
}

size_t StrLength(const char *string)
{
    if (string == NULL || cmp(string, ""))
        return 0;

    return strlen(string);
}


char *StrCopy(const char *string)
{
    if (string == NULL)
        return NULL;

    size_t size = StrLength(string) + 1;
    char *out = malloc(sizeof(char) * size);
    strcpy(out, string);

    // ensure the NULL char
    out[size - 1] = '\0';

    return out;
}


void StrCombine(char **out_str, const char *to_append)
{
    if (out_str == NULL || *out_str == NULL || to_append == NULL)
        return;

    size_t size = StrLength(*out_str) + StrLength(to_append) + 1; // +1 for '\0'

    *out_str = realloc(*out_str, size * sizeof *to_append);
    strcpy(*out_str + StrLength(*out_str), to_append);

    // ensure the NULL char
    (*out_str)[size - 1] = '\0';
}

char *IntToStr(const int x)
{
    // find out the size using NULL pointer first
    int len = snprintf(NULL, 0, "%d", x) + 1;

    // create a string using the found out length
    char* str = (char *) malloc(sizeof(char) * len);
    snprintf(str, len, "%d", x);

    // ensure the null char
    str[len - 1] = '\0';

    return str;
}



SymbolTable *NewSymbolTable()
{
    SymbolTable *result = (SymbolTable *) malloc(sizeof *result);
    result->length = 0;
    result->symbols = calloc(0, sizeof(Symbol));
    return result;
}

int Insert(SymbolTable *table, char *class, char *name, char *type, SymbolKind kind)
{
    if (LookUp(table, class, name) != NULL)
        return 0;

    // calculate id
    int id = 0;
    for (int i = 0; i < table->length; i++)
        if (table->symbols[i].kind == kind && cmp(table->symbols[i].class, class))
            id++;

    // resize the table
    table->symbols = realloc(table->symbols, sizeof(Symbol) * (++table->length));

    // insert new symbol
    table->symbols[table->length - 1].class = StrCopy(class);
    table->symbols[table->length - 1].name = StrCopy(name);
    table->symbols[table->length - 1].type = StrCopy(type);
    table->symbols[table->length - 1].kind = kind;
    table->symbols[table->length - 1].id = id;

    return 1;
}

Symbol *LookUp(SymbolTable *table, char *class, char *name)
{
    if (table == NULL)
        return NULL;

    for (int i = 0; i < table->length; i++)
        if (cmp(table->symbols[i].name, name) && cmp(table->symbols[i].class, class))
            return &table->symbols[i];

    return NULL;
}

void printTable(SymbolTable *table)
{
    printf("class\t\tkind\t\ttype\t\tname\t\tid\n");
    for (int i = 0; i < table->length; i++)
        printf("%s\t\t%d\t\t%s\t\t%s\t\t%d\n", table->symbols[i].class, table->symbols[i].kind, table->symbols[i].type, table->symbols[i].name, table->symbols[i].id);
}

int ClassExists(SymbolTable *table, char *class)
{
    for (int i = 0; i < table->length; i++)
        if (cmp(table->symbols[i].class, class))
            return 1;

    return 0;
}

SymbolTable *FilterTableByKind(SymbolTable *table, SymbolKind kind)
{
    SymbolTable *result = NewSymbolTable();
    for (int i = 0; i < table->length; i++)
    {
        Symbol s = table->symbols[i];
        if (s.kind == kind)
            Insert(result, s.class, s.name, s.type, s.kind);
    }
    return result;
}

SymbolTable *FilterTableByClass(SymbolTable *table, const char *class)
{
    SymbolTable *result = NewSymbolTable();
    for (int i = 0; i < table->length; i++)
    {
        Symbol s = table->symbols[i];
        if (cmp(s.class, class))
            Insert(result, s.class, s.name, s.type, s.kind);
    }
    return result;
}

TypeTable *NewTypeTable()
{
    TypeTable *result = (TypeTable *) malloc(sizeof *result);
    result->length = 0;
    result->types = calloc(0, sizeof(char *));

    return result;
}

void AddType(TypeTable *table, char *type)
{
    if (TypeExists(table, type))
        return;

    table->types = realloc(table->types, sizeof(char *) * (++table->length));
    table->types[table->length - 1] = StrCopy(type);
}

int TypeExists(TypeTable *table, char *type)
{
    for (int i = 0; i < table->length; i++)
        if (cmp(table->types[i], type))
            return 1;

    return 0;
}

MethodTables *NewMethodTables() {
    MethodTables *result = (MethodTables *) malloc(sizeof *result);
    result->length = 0;
    result->tables = calloc(0, sizeof(MethodTable));

    return result;
}

void AddMethodTable(MethodTables *tables, SymbolTable *table, const char *id)
{
    // resize the table
    tables->tables = realloc(tables->tables, sizeof(MethodTable) * (++tables->length));

    tables->tables[tables->length - 1].id = StrCopy(id);
    tables->tables[tables->length - 1].table = table;
}

SymbolTable *FindSymbolTable(MethodTables *tables, char *id)
{
    for (int i = 0; i<tables->length; i++)
        if (cmp(tables->tables[i].id, id))
            return tables->tables[i].table;
    return NULL;
}

ArgTable *NewArgTable()
{
    ArgTable *result = (ArgTable *) malloc(sizeof *result);
    result->length = 0;
    result->counts = calloc(0, sizeof(int));
    return result;
}

void AddArgCount(ArgTable *table, const char *id, int argCount)
{
    if (LookUpArgCount(table, id) != NULL)
        return;

    // resize the table
    table->counts = realloc(table->counts, sizeof(ArgCount) * (++table->length));

    // insert new arg count
    table->counts[table->length - 1].id = StrCopy(id);
    table->counts[table->length - 1].argCount = argCount;
}

ArgCount *LookUpArgCount(const ArgTable *table, const char *id)
{
    for (int i = 0; i < table->length; i++)
        if (cmp(table->counts[i].id, id))
            return &table->counts[i];
    return NULL;
}

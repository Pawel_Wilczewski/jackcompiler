#ifndef SYMBOLS_H
#define SYMBOLS_H

#include "lexer.h"
#include "parser.h"
#include "string.h"

// string utils
int cmp(const char *str1, const char *str2);
size_t StrLength(const char *string);
char *StrCopy(const char *string);
void StrCombine(char **out_str, const char *to_append);
char *IntToStr(const int x);

typedef enum {
    STATIC,
    FIELD,
    ARGUMENT,
    VAR,
    FUNCTION,
    METHOD,
    CONSTRUCTOR
} SymbolKind;

typedef struct {
    char *class;
    char *name;
    char *type;
    SymbolKind kind;
    int id;
} Symbol;

typedef struct {
    Symbol *symbols;
    int length;
} SymbolTable;

typedef struct {
    char **types;
    int length;
} TypeTable;

typedef struct {
    char *id;
    SymbolTable *table;
} MethodTable;

typedef struct {
    MethodTable *tables;
    int length;
} MethodTables;

typedef struct {
    char *id;
    int argCount;
} ArgCount;

typedef struct {
    ArgCount *counts;
    int length;
} ArgTable;

SymbolTable *NewSymbolTable();
// return false if symbol already existed, true otherwise
int Insert(SymbolTable *table, char *class, char *name, char *type, SymbolKind kind);
Symbol *LookUp(SymbolTable *table, char *class, char *name);
void printTable(SymbolTable *table);
int ClassExists(SymbolTable *table, char *class);
SymbolTable *FilterTableByKind(SymbolTable *table, SymbolKind kind);
SymbolTable *FilterTableByClass(SymbolTable *table, const char *class);

MethodTables *NewMethodTables();
void AddMethodTable(MethodTables *tables, SymbolTable *table, const char *id);
SymbolTable *FindSymbolTable(MethodTables *tables, char *id);

// automatically with generic types
TypeTable *NewTypeTable();
void AddType(TypeTable *table, char *type);
int TypeExists(TypeTable *tab, char *type);

ArgTable *NewArgTable();
void AddArgCount(ArgTable *table, const char *id, int argCount);
ArgCount *LookUpArgCount(const ArgTable *table, const char *id);

#endif
